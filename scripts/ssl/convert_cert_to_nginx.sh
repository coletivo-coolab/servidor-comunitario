#!/bin/bash

read -e -p "Digite um domínio para converter o certificado para nginx:" DOMAIN
mv fullchain.pem $DOMAIN.crt
openssl pkey -in privkey.pem -out $DOMAIN.key
