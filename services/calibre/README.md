Editar o arquivo de configuração, especialmente
  - domínio (DOMAIN)
  - hora (TIMEZONE)
  - usuario e grupo (PUID e PGID)

Antes de tudo, para descobrir seu PUID e PGID, digite:
  id $USER
$USER é a usuária que executará docker-compose.

Retornará algo similar à:
```uid=1000(admina) gid=1000(admina) Gruppen=1000(admina), 1001(docker)```
Onde **uid** e **gid** são os numeros correspondentes à **PUID** e **PGID**.

Copiar o arquivo de configuração, digite:
  cp .env.example .env

Editar o arquivo copiado:
  nano .env

Depois de salva as informações, inicie o container, com o seguinte comando:
  docker-compose up -d

Leva um tempinho para iniciar, pode buscar uma água enquanto isso ;)
Agora faça o teste no seu navegador, no nosso caso, digitando
**http://biblioteca.rede.comunitaria**

Caso não esteja funcionando, verifique o que anda com o container, verificando
o seu estado, com o comando:
  docker ps

Se aparentemente tá tudo bem (terá um Up na linha calibre-web), verifique se
existe algum erro através do comando:
  docker logs biblioteca.rede.comunitaria
Onde **biblioteca.rede.comunitaria** é o nome do domínio que você escolheu.

No ultimo dos casos, reinicie seu computador e teste novamente.
