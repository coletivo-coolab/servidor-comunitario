# Nextcloud - sua própria nuvem

Para inicializar esse serviço:

Copie o arquivo de configurações:
  ```cp .env.example .env```

Edite as configurações, especialmente senhas (PASSWORD) e o domínio (DOMAIN):
  ```nano .env```

Inicialize o serviço, subindo o container:
  ```docker-compose up -d```

Caso queira testar no seu computador, adicione o domínio que você criou ao arquivo de direções (atenção, precisará de super poderes para isso), por exemplo `127.0.0.0.1   minha-nuvem.org`
  ```sudo nano /etc/hosts```

Faça o teste no seu navegador, digitando o endereço (DOMAIN) criado.
