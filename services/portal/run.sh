#!/bin/bash

if [ -f .env ]
then
    export $(echo $(cat .env | sed 's/#.*//g'| xargs) | envsubst)
    curl -L https://gitlab.com/coletivo-coolab/portal-nuxt/-/archive/main/portal-nuxt-main.zip -o portal-nuxt.zip
    curl -L https://github.com/moinhodigital/tile-server/archive/master.zip -o tile-server.zip
    unzip portal-nuxt.zip
    unzip tile-server.zip
    mkdir -p $PORTAL_DIR
    mv portal-nuxt-main/* $PORTAL_DIR/
    cp $PORTAL_DIR/content/index-example.md $PORTAL_DIR/content/index.md
    sudo chown -R 777 $PORTAL_DIR
    docker-compose up -d

else
    echo "Criei um arquivo .env e adicione variaveis"
fi
