version: '3.5'
services:

  db:
    image: postgres:11
    container_name: mirrorbrain-db
    volumes:
      - "${DIR}/kiwix/db:/var/lib/postgresql/data"
    command: postgres -c shared_buffers=256MB -c max_connections=200
    restart: always

  mirrorbrain-backup:
    image: kiwix/borg-backup
    container_name: mirrorbrain-backup
    links:
      - db
    env_file:
      - .env_mirrorbrain_backup
    restart: always

  web:
    image: kiwix/mirrorbrain
    container_name: mirrorbrain-web
    volumes:
      - "${DIR}/kiwix/download:/var/www/download.kiwix.org"
      - "${DIR}/kiwix/library:/var/www/library.kiwix.org"
      - "${DIR}/kiwix/hash:/usr/share/mirrorbrain"
      - "${DIR}/kiwix/geoip:/usr/local/geoip/share/GeoIP"
    environment:
      - UPDATE_HASH=1
      - HTTPD=1
      - VIRTUAL_HOST=download.kiwix.org
      - LETSENCRYPT_HOST=download.kiwix.org
      - LETSENCRYPT_EMAIL=contact@kiwix.org
      - HTTPS_METHOD=noredirect
    networks:
      default:
        aliases:
          - download
    links:
      - db
    restart: always

  download-backup:
    image: kiwix/borg-backup
    container_name: download-backup
    volumes:
      - "${DIR}/kiwix/openzim/README:/storage/openzim/README:ro"
      - "${DIR}/kiwix/openzim/robots.txt:/storage/openzim/robots.txt:ro"
      - "${DIR}/kiwix/openzim/archive:/storage/openzim/archive:ro"
      - "${DIR}/kiwix/openzim/release:/storage/openzim/release:ro"
      - "${DIR}/kiwix/openzim/wikifundi:/storage/openzim/wikifundi:ro"
      - "${DIR}/kiwix/download/README:/storage/kiwix/README:ro"
      - "${DIR}/kiwix/download/favicon.ico:/storage/kiwix/favicon.ico:ro"
      - "${DIR}/kiwix/download/robots.txt:/storage/kiwix/robots.txt:ro"
      - "${DIR}/kiwix/download/archive:/storage/kiwix/archive:ro"
      - "${DIR}/kiwix/download/dev:/storage/kiwix/dev:ro"
      - "${DIR}/kiwix/download/other:/storage/kiwix/other:ro"
      - "${DIR}/kiwix/download/release:/storage/kiwix/release:ro"
      - "${DIR}/kiwix/download/screenshots:/storage/kiwix/screenshots:ro"
    env_file:
      - .env_download_backup
    restart: always

  update-db:
    image: kiwix/mirrorbrain
    container_name: mirrorbrain-update
    volumes:
      - "${DIR}/kiwix/download:/var/www/download.kiwix.org"
      - "${DIR}/kiwix/openzim:/var/www/download.openzim.org"
    environment:
      - UPDATE_DB=1
    links:
      - db
    restart: always

  library:
    image: kiwix/library
    container_name: library
    volumes:
      - "${DIR}/kiwix/library:/var/www/library.kiwix.org"
      - "${DIR}/kiwix/download:/var/www/download.kiwix.org"
    environment:
      - VIRTUAL_HOST=${DOMAIN}
      # - HTTPS_METHOD=noredirect
    secrets:
      - wiki-password
    restart: always

  # reverse-proxy:
  #   image: kiwix/reverse-proxy
  #   container_name: reverse-proxy
  #   volumes:
  #     - "/var/run/docker.sock:/tmp/docker.sock:ro"
  #     - "vhost:/etc/nginx/vhost.d"
  #     - "${DIR}/kiwix/html:/usr/share/nginx/html"
  #     - "${DIR}/kiwix/openzim:/var/www/download.openzim.org"
  #     - "${DIR}/kiwix/download:/var/www/download.kiwix.org"
  #     - "${DIR}/kiwix/library:/var/www/library.kiwix.org"
  #     - "${DIR}/kiwix/tmp:/var/www/tmp.kiwix.org"
  #     - "${DIR}/kiwix/certs:/etc/nginx/certs:ro"
  #     - "${DIR}/kiwix/log/nginx:/var/log/nginx"
  #   environment:
  #     - VIRTUAL_HOST=download.openzim.org,mirror.download.kiwix.org,tmp.kiwix.org
  #     - LETSENCRYPT_HOST=download.openzim.org,mirror.download.kiwix.org,tmp.kiwix.org
  #     - LETSENCRYPT_EMAIL=contact@kiwix.org
  #     # - HTTPS_METHOD=noredirect
  #   labels:
  #     - com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy
  #   ports:
  #     - "80:80"
  #     - "443:443"
  #   networks:
  #     default:
  #       aliases:
  #         - mirror.download
  #         - tmp
  #   restart: always

  # letsencrypt:
  #   image: jrcs/letsencrypt-nginx-proxy-companion
  #   container_name: letsencrypt
  #   depends_on:
  #     - reverse-proxy
  #   volumes:
  #     - "/var/run/docker.sock:/var/run/docker.sock:ro"
  #     - "vhost:/etc/nginx/vhost.d"
  #     - "${DIR}/kiwix/html:/usr/share/nginx/html"
  #     - "${DIR}/kiwix/certs:/etc/nginx/certs:rw"
  #   restart: always

  ftpd:
    image: gimoh/pureftpd
    container_name: ftpd
    ports:
      - "21:21"
      - "30000-30050:30000-30050"
    volumes:
      - "${DIR}/kiwix/download:/var/lib/ftp"
    entrypoint:
      - /usr/local/sbin/dkr-init
      - -p
      - "30000:30050"
    restart: always

  rsyncd:
    image: kiwix/rsyncd
    container_name: rsyncd
    ports:
      - "873:873"
    volumes:
      - "${DIR}/kiwix/download:/var/www/download.kiwix.org"
      - "${DIR}/kiwix/openzim:/var/www/download.openzim.org"
      - "${DIR}/kiwix/tmp:/var/www/tmp.kiwix.org"
    restart: always

  zimfarm-receiver:
    image: openzim/zimfarm-receiver
    container_name: zimfarm-receiver
    volumes:
      - "${DIR}/kiwix/zimfarm-warehouse/logs:/jail/logs:rw"
      - "${DIR}/kiwix/zimfarm-warehouse/zim:/jail/zim:rw"
      - "${DIR}/kiwix/log/zimquarantine:/mnt/check_logs:rw"
      - "${DIR}/kiwix/zimfarm-warehouse/quarantine:/mnt/quarantine:rw"
      - "${DIR}/kiwix/download/zim:/mnt/zim:rw"
    environment:
      - VALIDATION_OPTION=NO_CHECK
    ports:
      - "1522:22"
    restart: always

  matomo-download:
    image: kiwix/matomo-log-analytics
    container_name: matomo-log-analytics_download
    volumes:
      - "${DIR}/kiwix/log/nginx:/var/log/nginx"
    environment:
      - ID_SITE=2
      - URL=http://stats.kiwix.org
      - LOG=/var/log/nginx/access.log
      - HOST=download.kiwix.org
    secrets:
      - matomo-token
    restart: always
  matomo-download_openzim:
    image: kiwix/matomo-log-analytics
    container_name: matomo-log-analytics_download_openzim
    volumes:
      - "${DIR}/kiwix/log/nginx:/var/log/nginx"
    environment:
      - ID_SITE=6
      - URL=http://stats.kiwix.org
      - LOG=/var/log/nginx/access.log
      - HOST=download.openzim.org
    secrets:
      - matomo-token
    restart: always

volumes:
  vhost:
secrets:
  matomo-token:
    file: ./matomo-token.txt
  wiki-password:
    file: ./wiki-password.txt
networks:
  default:
    name: kiwix.org

