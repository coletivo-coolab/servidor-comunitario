#!/bin/bash

ssh-keygen -t rsa
echo "On computer use ssh-copy-id to copy the key to this machine:"
echo "ssh-copy-id -i ~/.ssh/id_rsa.pub ${USER}@${IP}"