#!/bin/bash

# From: https://www.raspberrypi.org/documentation/configuration/wireless/access-point-routed.md

# Install
sudo apt update
sudo apt upgrade -y
sudo apt install dnsmasq hostapd netfilter-persistent iptables-persistent -y
echo "Starting up..."
sudo systemctl unmask hostapd
sudo systemctl enable hostapd
# Setup
echo "Firstly, the server’s WiFi interface wlan0 needs to have a predictable IP address and not try to obtain it from another server. We call this a static IP."
echo "Modificando /etc/dhcpcd.conf"
cat dhcpd.conf | sudo tee -a /etc/dhcpcd.conf
# cat /etc/dhcpcd.conf
echo "We create a new configuration file for dnsmasq in the appropriate location and start editing it:"
echo "Modificando /etc/sysctl.d/routed-ap.conf"
cat routed-ap.conf | sudo tee /etc/sysctl.d/routed-ap.conf

sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo netfilter-persistent save

echo "The DHCP and DNS services are provided by dnsmasq"
echo "Modificando /etc/dnsmasq.conf"
cat dnsmasq.conf | sudo tee /etc/dnsmasq.conf

sudo rfkill unblock wlan

echo "We'll need to write a configuration file with information about your local Wi-Fi network."
echo "Modificando /etc/default/hostapd"
cat hostapd.conf | sudo tee /etc/hostapd/hostapd.conf

sudo systemctl start hostapd
sudo systemctl restart dnsmasq
sudo systemctl reboot

