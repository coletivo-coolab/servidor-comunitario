#!/bin/bash

# Should run on a cloud server where the desired domain dns-server is direct too

REQUIRED_PKG="certbot"
PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $REQUIRED_PKG|grep "install ok installed")
echo Checking for $REQUIRED_PKG: $PKG_OK
if [ "" = "$PKG_OK" ]; then
  echo "No $REQUIRED_PKG. Setting up $REQUIRED_PKG."
  sudo apt-get --yes install $REQUIRED_PKG
fi

read -e -p "Digite um domínio para gerar um certificado wildcard:" DOMAIN
read -e -p "Digite o email que vai estar vinculado ao certificado:" EMAIL

sudo certbot certonly \
    --manual \
    --preferred-challenges=dns \
    --email $EMAIL \
    --server https://acme-v02.api.letsencrypt.org/directory \
    --agree-tos \
    -d $DOMAIN -d *.$DOMAIN
