Mucua eh uma semente da rede [Baobaxia](https://labmocambos.taina.net.br/npdd/baobaxia-mucua/)
Por enquanto temos a mucua disponivel mas ainda nos falta uma carinha pra ela

Copie o arquivo de variaves:
$ cp .env.example .env

Edite o arquivo de variaveis, colocando os valores que vc precisa:
$ nano .env

Salve o arquivo e saia do editor.

Inicie sua mucua
$ docker-compose up -d
