#!/bin/bash

sudo systemctl disable dnsmasq
sudo systemctl disable hostapd

# IPTABLES
sudo iptables -P INPUT ACCEPT
sudo iptables -P OUTPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -F