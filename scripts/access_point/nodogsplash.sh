#!/bin/bash

sudo apt install git libmicrohttpd-dev -y
cd ~
git clone https://github.com/nodogsplash/nodogsplash.git
cd ~/nodogsplash
make
sudo make install
cat nodogsplash.conf | sudo tee -a /etc/nodogsplash/nodogsplash.conf
sudo nodogsplash
sudo sed -i '/exit 0/i \
nodogsplash' /etc/rc.local

